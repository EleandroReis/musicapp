if ("serviceWorker" in navigator) {
  window.addEventListener("load", function() {
    navigator.serviceWorker.register("/sw.js").then(
      function(registration) {
        // Registration was successful
        console.log(
          "ServiceWorker registration successful with scope: ",
          registration.scope
        );
      },
      function(err) {
        // registration failed :(
        console.log("ServiceWorker registration failed: ", err);
      }
    );
  });
}

window.onload = async function() {
  const musicData = await getMusicData();
  const previous = document.querySelector("#previous");
  const next = document.querySelector("#next");
  const buttonBack = document.querySelector(".back");
  const buttonPlayAndPause = document.querySelector("#playAndPause");
  let musicIndex = 0;

  renderPlaylist(musicData);

  const songs = document.querySelectorAll(".playlist a");

  songs.forEach((song, index) => {
    song.addEventListener("click", function() {
      const songId = song.getAttribute("data-music-id");
      const songDetails = musicData.filter(item => item.id === songId)[0];
      musicIndex = index;
      renderPlayingScreen(songDetails);
    });
  });

  previous.addEventListener("click", function() {
    const currentTrack = musicData[musicIndex];
    let index = musicIndex > 0 ? musicIndex - 1 : musicData.length - 1;
    const previousTrack = musicData[index];
    document
      .querySelector(`[data-music-id="${currentTrack.id}"]`)
      .classList.remove("class", "playing");
    document
      .querySelector(`[data-music-id="${previousTrack.id}"]`)
      .classList.add("class", "playing");
    musicIndex = index;
    renderPlayingScreen(previousTrack);
  });

  next.addEventListener("click", function() {
    const currentTrack = musicData[musicIndex];
    let index = musicIndex < musicData.length - 1 ? musicIndex + 1 : 0;
    const nextTrack = musicData[index];
    document
      .querySelector(`[data-music-id="${currentTrack.id}"]`)
      .classList.remove("class", "playing");
    document
      .querySelector(`[data-music-id="${nextTrack.id}"]`)
      .classList.add("class", "playing");
    musicIndex = index;
    renderPlayingScreen(nextTrack);
  });

  buttonBack.addEventListener("click", function() {
    document.querySelector(".playing_box").classList.remove("active");
  });

  buttonPlayAndPause.addEventListener("click", function() {
    audio = document.querySelector("audio");
    if (audio.paused) {
      document.querySelector("#playAndPause i").innerHTML =
        "pause_circle_filled";
      audio.play();
    } else {
      document.querySelector("#playAndPause i").innerHTML =
        "play_circle_filled";
      audio.pause();
    }

    // GOOGLE CAST
    var player = new cast.framework.RemotePlayer();
    var playerController = new cast.framework.RemotePlayerController(player);
    playerController.playOrPause();
  });

  navigator.mediaSession.setActionHandler("play", function() {
    const audio = document.querySelector("audio");
    document.querySelector("#playAndPause i").innerHTML = "pause_circle_filled";
    audio.play();
  });

  navigator.mediaSession.setActionHandler("pause", function() {
    const audio = document.querySelector("audio");
    document.querySelector("#playAndPause i").innerHTML = "play_circle_filled";
    audio.pause();
  });

  navigator.mediaSession.setActionHandler("previoustrack", function() {
    previous.click();
  });

  navigator.mediaSession.setActionHandler("nexttrack", function() {
    next.click();
  });

  const btnAdd = document.querySelector(".btnAdd");

  let deferredPrompt;

  window.addEventListener("beforeinstallprompt", e => {
    e.preventDefault();
    deferredPrompt = e;
    btnAdd.style.display = "block";
  });

  btnAdd.addEventListener("click", e => {
    deferredPrompt.prompt();
    deferredPrompt.userChoice.then(choiceResult => {
      if (choiceResult.outcome === "accepted") {
        console.log("User accepted the A2HS prompt");
      }
      deferredPrompt = null;
    });
  });

  window.addEventListener("appinstalled", evt => {
    app.logEvent("a2hs", "installed");
  });
};

function updateMetadata(data) {
  navigator.mediaSession.metadata = new MediaMetadata({
    title: data.song,
    artist: data.artist,
    album: data.album,
    artwork: data.artwork
  });
}

function getMusicData() {
  return fetch("/music.json")
    .then(response => response.json())
    .then(resp => resp);
}

function renderPlayingScreen(data) {
  const wrapperPlayingScreen = document.querySelector(".playing_box");
  const musicCountUp = document.querySelector(".timeline_init");
  const musicDuration = document.querySelector(".timeline_final");
  const playPauseIcon = document.querySelector("#playAndPause i");
  const audio = document.querySelector("audio");
  audio.setAttribute("src", data.source);
  audio
    .play()
    .then(_ => updateMetadata(data))
    .catch(error => console.log(error));

  // Countup
  audio.addEventListener(
    "timeupdate",
    function() {
      var s = parseInt(audio.currentTime % 60);
      var m = parseInt((audio.currentTime / 60) % 60);

      const minutes = Math.floor(parseInt(audio.duration) / 60) % 60;
      const seconds = parseInt(audio.duration % 60);

      const point = document.querySelector(".timeline span");
      const currentTime =
        (parseInt(audio.currentTime) * 100) / parseInt(audio.duration);
      point.style.left = `${currentTime}%`;
      if (s < 10) {
        musicCountUp.innerHTML = m + ":0" + s;
      } else {
        musicCountUp.innerHTML = m + ":" + s;
      }
      musicDuration.innerHTML = `${minutes}:${seconds}`;
    },
    false
  );

  const el = document.querySelector(".playlist a.playing");
  if (el) {
    el.classList.remove("class", "playing");
  }
  document
    .querySelector(`[data-music-id="${data.id}"]`)
    .classList.add("class", "playing");
  document
    .querySelector(".playing_card img")
    .setAttribute("src", data.artwork[3].src);
  document.querySelector(".playing_card h3").innerHTML = data.song;
  document.querySelector(".playing_card p").innerHTML = data.artist;
  playPauseIcon.innerHTML = "pause_circle_filled";

  wrapperPlayingScreen.classList.add("class", "active");
}

function renderPlaylist(playlist) {
  const elemPlaylist = document.querySelector(".playlist");
  let wrapperHtml = "";
  playlist.map(music => {
    wrapperHtml += `<a href="#" data-music-id="${music.id}">
            <img src="${music.artwork[2].src}" />
            <div class="details">
                <h3>${music.song}</h3>
                <p>${music.artist}</p>
                <small><i class="material-icons">access_time</i> 03:45</small>
            </div>
        </a>`;
  });

  elemPlaylist.innerHTML = wrapperHtml;
}
