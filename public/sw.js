var CACHE_NAME = 'cache-v1';
var urlsToCache = [
  '/',
  '/assets/css/app.css',
  '/assets/js/app.js',
  '/assets/img',
  'https://fonts.googleapis.com/css?family=Open+Sans:400,700&display=swap',
  'https://fonts.googleapis.com/icon?family=Material+Icons',
  '/music.json'
];

self.addEventListener('install', function(event) {
  // Perform install steps
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function(cache) {
        console.log('Opened cache');
        return cache.addAll(urlsToCache);
      })
  );
});

self.addEventListener('fetch', function(event) {
    event.respondWith(
      caches.match(event.request)
        .then(function(response) {
          // Cache hit - return response
          if (response) {
            return response;
          }
          return fetch(event.request);
        }
      )
    );
});