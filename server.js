const express = require("express");
const path = require("path");
const port = process.env.PORT || 8080;
const app = express();
const bodyParser = require("body-parser");
const axios = require("axios");

// the __dirname is the current directory from where the script is running
app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, "public")));

// Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/*", function(req, res) {
  res.sendFile(path.join(__dirname, "public", "index.html"));
});

app.post("/webhooks", (req, res) => {
  axios
    .post(
      "https://api.telegram.org/bot656638296:AAE6cpVaWHqpNRQ9y3fct7Fhi4VqQ_QefEw/sendMessage",
      {
        chat_id: "832855645",
        text: `${req.body.app} app was deployed! Commit: ${
          req.body.head
        } User: ${req.body.user} Gitlog: ${req.body.git_log} Visit: ${
          req.body.url
        }`
      }
    )
    .then(res => {
      console.log(res);
    })
    .catch(error => {
      console.error(error);
    });
});

app.listen(port);
